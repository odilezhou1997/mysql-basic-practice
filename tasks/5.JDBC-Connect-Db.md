# 使用JDBC链接数据库
## Tasks
1. 使用JDBC链接到第一节创建的数据库上面。
2. 实现学生表的增删改查操作。

## 你该怎么做：
1. Fork或Clone这个仓库[jdbc-practise](https://gitlab.com/tw-baseline-exam/jdbc-practice)
2. 基于此仓库实现学生表的增删改查操作。
3. 请认真阅读该仓库的Readme文档，按照要求进行。
